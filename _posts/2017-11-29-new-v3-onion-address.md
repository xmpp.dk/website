---
title: New v3 Onion Address
layout: post
---
The Tor project is fast at work on the next generation of Tor. As a part of that, a complete re-vamp of [how Onion services work](https://blog.torproject.org/tors-fall-harvest-next-generation-onion-services) is on the table.  
  
To help out in testing and also to gain some experience with this new technology, I've enabled it on xmpp.dk.  
  
The v3 Onion address to connect to our services is **3hbedm3irbcgr6yanknkeizk7bjetlw5tlulooz73w2w3dgeptm7zeid.onion**.  
The old address **mjfjbs3547exsy6r.onion** still works, so you can continue to use that if you're not quite ready to run pre-release versions of Tor.  
  
Please give it a try and let me know if you experience any issues. 