#!/bin/bash

echo RENEWED_DOMAINS=$RENEWED_DOMAINS
if [[ $RENEWED_DOMAINS = *xmpp.dk* ]]
then
  echo "Running certificate post-update script"
  cp /etc/letsencrypt/live/xmpp.dk/* /etc/prosody/certs
  chown prosody:prosody /etc/prosody/certs/*

  SHA256=$(openssl x509 -noout -fingerprint -sha256 -inform pem -in /etc/letsencrypt/live/xmpp.dk/cert.pem | cut -c20-)
  SHA1=$(openssl x509 -noout -fingerprint -sha1 -inform pem -in /etc/letsencrypt/live/xmpp.dk/cert.pem | cut -c18-)
  EXPIRES=$(openssl x509 -noout -enddate -inform pem -in /etc/letsencrypt/live/xmpp.dk/cert.pem | cut -c10-)

  rm -fr /tmp/xmpp.dk
  git clone /home/graffen/website.git /tmp/xmpp.dk
  sed -i "s/sha-256:.*/sha-256: $SHA256/" /tmp/xmpp.dk/_config.yml
  sed -i "s/sha-1:.*/sha-1: $SHA1/" /tmp/xmpp.dk/_config.yml
  sed -i "s/expiry:.*/expiry: $EXPIRES/" /tmp/xmpp.dk/_config.yml
  #rm -fr /tmp/xmpp.dk

  git -C /tmp/xmpp.dk/ add _config.yml
  git -C /tmp/xmpp.dk/ commit -m"Automated update of certificate info" 2&>1 /dev/null
  git -C /tmp/xmpp.dk/ push

  systemctl reload prosody
  systemctl reload nginx
fi

